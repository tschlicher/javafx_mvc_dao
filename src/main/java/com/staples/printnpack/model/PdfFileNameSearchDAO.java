package com.staples.printnpack.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import Util.DBUtil;

/**
 *     Author: Thomas Schlicher
 *       Date: 2 May 2019
 *   
 *   comments: This class will be used to hold methods that will execute CRUD DB Operations on the ST_WMS_Carton and ST_WMS_PACKINGSLIP tables.
 *   			 
 */

public class PdfFileNameSearchDAO 
{
	
	//*******************************
    //SELECT a Carton
    //*******************************
    public static PdfFileNameSearch searchWesCartonDataTracking (String barCode) throws SQLException, ClassNotFoundException 
    {
        //Declare a SELECT statement
    	// SoCal DB
       // String selectStmt = "SELECT * FROM PrintnPack.dbo.carton_data WHERE Barcode='"+barCode+"'";
        
        // Beloit DB
        //String selectStmt = "SELECT * FROM STB_OMS.dbo.ST_WMS_CARTON WHERE Barcode='" + barCode + "'";
        
        // Dallas
    	//String selectStmt = "SELECT * FROM STCP_OMS.dbo.ST_WMS_CARTON WHERE Barcode='" + barCode + "'";
    	
    	// Lithia
    	//String selectStmt = "SELECT * FROM STL_OMS.dbo.ST_WMS_CARTON WHERE Barcode='" + barCode + "'";
    	
    	// Charlotte
    	String selectStmt = "SELECT * FROM STCL_OMS.dbo.ST_WMS_CARTON WHERE Barcode='" + barCode + "'";
 
        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = DBUtil.dbExecutePdfQuery(selectStmt);
 
            //Send ResultSet to the getOrderFromResultSet method and get orders object
            PdfFileNameSearch carton = getWesOrderFromResultSet(rsEmp);
 
            //Return wes object
            return carton;
        } catch (SQLException e) {
            System.out.println("While searching an order with " + barCode + " barcode, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    
  //Use ResultSet from DB as parameter and set carton data Object's attributes and return order object.
    private static PdfFileNameSearch getWesOrderFromResultSet(ResultSet rs) throws SQLException
    {
    	PdfFileNameSearch ord = null;
        if (rs.next()) {
        	ord = new PdfFileNameSearch();
        	ord.setBarcode(rs.getString("Barcode"));
        }
        return ord;
    }
    
	//*************************************************
    //Retrieve PDF file name based on Id and carton Id
    //*************************************************
    public static String retrievePdfFileName(String pdfBarcode)
    {
    	// Beloit
    	/*String selectStmt = "Select P.FileName\r\n" + 
    			"FROM STB_OMS.dbo.ST_WMS_Carton as C, STB_OMS.dbo.ST_WMS_PACKSLIP as P\r\n" + 
    			"WHERE C.Id = P.CartonId AND C.Barcode = '"+pdfBarcode+"';";*/
    	
    	// Dallas
    	/*String selectStmt = "Select P.FileName\r\n" + 
    			"FROM STCP_OMS.dbo.ST_WMS_Carton as C, STCP_OMS.dbo.ST_WMS_PACKSLIP as P\r\n" + 
    			"WHERE C.Id = P.CartonId AND C.Barcode = '"+pdfBarcode+"';";*/
    	
    	// Lithia
    	//String selectStmt = "Select P.FileName\r\n" + 
    		//	"FROM STL_OMS.dbo.ST_WMS_Carton as C, STL_OMS.dbo.ST_WMS_PACKSLIP as P\r\n" + 
    			//"WHERE C.Id = P.CartonId AND C.Barcode = '"+pdfBarcode+"';";
    	
    	// Charlotte
    	String selectStmt = "Select P.FileName\r\n" + 
    			"FROM STCL_OMS.dbo.ST_WMS_Carton as C, STCL_OMS.dbo.ST_WMS_PACKSLIP as P\r\n" + 
    			"WHERE C.Id = P.CartonId AND C.Barcode = '"+pdfBarcode+"';";
    	
    	String file = null;
    	
    	//Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
        	System.out.println("Executing query");
            ResultSet rsEmp = DBUtil.dbExecutePdfQuery(selectStmt);
 
            //Send ResultSet to the getOrderFromResultSet method and get orders object
            file = getOrderFromResultSet(rsEmp);
 
            //Return employee object
            return file;
        } catch (SQLException e) {
            System.out.println("While searching an order with " + pdfBarcode + " barcode, an error occurred: " + e);
            //Return exception
            //throw e;
        } catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file;
    }
    
  //Use ResultSet from DB as parameter and set carton data Object's attributes and return order object.
    private static String getOrderFromResultSet(ResultSet rs) throws SQLException
    {
    	String myFile = null;

        if (rs.next()) {
        	myFile = rs.getString("FileName");
        	System.out.println("Retrieved file name : " + myFile);
        }
        return myFile;
    }
}
