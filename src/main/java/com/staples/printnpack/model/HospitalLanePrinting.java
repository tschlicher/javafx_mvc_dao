package com.staples.printnpack.model;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.*;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ProtocolCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.staples.printnpack.PropertiesCache;
import com.staples.printnpack.controller.MainWindowController;

/**
 * Comments: This class makes use for FTP to retrieve a file that will be read in via a scanner. Once we have the correct file,
 * 			 we will then print out the information needed to the hospital lane printer.
 * 
 * 
 * @author SchTh001
 *
 */

public class HospitalLanePrinting 
{
	private static final Logger LOGGER = LogManager.getLogger(HospitalLanePrinting.class.getName());
	
	// Initializing string
	String str = "";
	FTPClient ftp = null;
	File pdfFile = null;
	InputStream inStream = null;
	
	String ftpServer = PropertiesCache.getInstance().getProperty("ftp.server.address");
	String ftpRemoteFilePath = PropertiesCache.getInstance().getProperty("retreive.ftp.files");
	String ftpUserName = PropertiesCache.getInstance().getProperty("ftp.username");
	String ftpPassword = PropertiesCache.getInstance().getProperty("ftp.password");
	
	public boolean searchPdfAndPrint(String barcode) throws SocketException, IOException
	{	
		// NON WES sites FTP
		// return ftpClient(barcode);
		
		// WES Sites Network folder
		return readFile(barcode);
	}
	
	/**
	 * Comments: This method will handle the FTP to retrieve a file for printing.
	 * 
	 * @param getBarcode
	 * @return 
	 * @throws IOException 
	 * @throws SocketException 
	 */
	private Boolean ftpClient(String getBarcode) throws SocketException, IOException
	{
		Boolean bool = false;
		String pdfBarcode = getBarcode + ".PDF";

			ftp = new FTPClient();
			ftp.addProtocolCommandListener((ProtocolCommandListener) new PrintCommandListener(new PrintWriter(System.out)));
	        int reply;
	        
	        ftp.connect(ftpServer);
	        
	        reply = ftp.getReplyCode();
	        
	        if (!FTPReply.isPositiveCompletion(reply)) 
	        {
	            ftp.disconnect();
	        }
	        
	        ftp.login(ftpUserName, ftpPassword);
	       
	        ftp.setFileType(FTP.BINARY_FILE_TYPE);
	        ftp.enterLocalPassiveMode();	    
	        
	        ftp.changeWorkingDirectory(ftpRemoteFilePath);	        
			
			FTPFile[] files = ftp.listFiles();
	        for (FTPFile file : files) 
	        {	 
	            if(file.isFile() && file.getName().equalsIgnoreCase(pdfBarcode))
	            {           	
	            	// File found
	            	inStream = ftp.retrieveFileStream(file.getName());
	            	File pdfFile = File.createTempFile("tmp", null);
	    	        FileUtils.copyInputStreamToFile(inStream, pdfFile);
	    	        System.out.println("Sending file to printer method");
	    	        printPdfJob(pdfFile);
		    	     
	    	        inStream.close();
	            	bool = true;
	            	return bool;
	            }
	        }	      
		
	        if (this.ftp.isConnected()) 
	        {
	        	try {
	                this.ftp.logout();
	                this.ftp.disconnect();
	            } catch (IOException f) {
	                // do nothing as file is already downloaded from FTP server
	            }
	        }
		return bool;	
	}
	
	/**
	 * Comments: This method will be used for FTP file searching on WES Sites only e.g. Beloit, Lithia, Dallas
	 * 
	 * @param ftpFilename
	 * @return
	 * @throws UnknownHostException 
	 * @throws IOException 
	 */
	private boolean readFile(String ftpFilename) throws UnknownHostException, IOException
	{
		Boolean bool = false;
		
		System.out.println("Looking for pdf file");
		String correctPDF = PdfFileNameSearchDAO.retrievePdfFileName(ftpFilename);
		if(networkFile(correctPDF))
		{
			System.out.println("************************************************************************File found Successfully");
			bool = true;
			return bool;
		}else {
			System.out.println("***************************************************************************File not found Successfully");
			return bool;
		}
	}
	
	/**
	 * Comments: Search for FTP file and print packing slip
	 * 
	 * @param ftpFilename
	 * @return
	 * @throws UnknownHostException 
	 * @throws IOException 
	 */
	private boolean networkFile(String ftpFilename) throws UnknownHostException, IOException
	{
		Boolean bool = false;

		// Beloit FTP
		//File getNetworkFile = new File("\\\\WCVYPRDBV05\\SharedPackSlipFolder\\" +ftpFilename);
		
		// Dallas FTP
		//File getNetworkFile = new File("\\\\WCVYPRDBV06\\SharedPackSlipFolder\\" +ftpFilename);
		
		// Dallas FTP
		//File getNetworkFile = new File("\\\\WCVYPRDBV04\\SharedPackSlipFolder\\" +ftpFilename);
		
		// Charlotte
		File getNetworkFile = new File("\\\\WCVYPRDBV08\\SharedPackSlipFolder\\" +ftpFilename);
	
		// See if the getNetworkFile does not exist - return false for non existent and true for exist
		if(!(getNetworkFile.exists()))
		{
			// File not found
			System.out.println("********************************************************************File not found : " + getNetworkFile.getName());
        	return bool;
		}else {
			System.out.println("**********************************************************************File found: " + getNetworkFile.getName());
			printPdfJob(getNetworkFile);
			bool = true;
			return bool;
		}    
	}
		
	/**
	 * Comments: This method will be used to handle the reading of a file and the printing.
	 *     Date: 4 March 2019
	 * 
	 * 			 Steps for printing:
	 * 
	 * 				1) Read in the FTP file using FileInputStream
	 * 				2) Set up our DocFlavor for our input stream
	 * 				3) Set style of our Doc (SimpleDoc) etc
	 * 				4) Get the PrintRequestAttributeSet
	 * 				5) Do a look up on all printers that will be on the network
	 * 				6) Find match on printer service and verify that it's not null
	 * 				7) Create DocPrintJob with the printer we've found
	 * 				8) Call PrintJobStatus class and pass in our print job (document and attributes)
	 * 				9) Call waitForDone() method that resides in PrintJobStatus and return a statuc code
	 * 				10) Close inputStream and return status code
	 * 
	 * @param getFile
	 * @return
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public void printPdfJob(File getFile) throws UnknownHostException, IOException
	{		
		// Create client  socket connection to printer
		Socket socket = null;	
		
		// Initializing FileInputStream
		FileInputStream inStream = null;
		String getSocket = PropertiesCache.getInstance().getProperty("retrieve.socket");

		try {
        	socket = new Socket(getSocket, 9100);
        	
        	System.out.println("Socket created");
        	
            // create FileInputStream object
        	inStream = new FileInputStream(getFile);
 
            byte[] pdfContent = new byte[(int)getFile.length()];
            System.out.println("******************************************************************************Convert PDF file to bytes");
             
            // Reads bytes of data from the input stream into an array of bytes.
            inStream.read(pdfContent);
            
            
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			dos.write(pdfContent);
			System.out.println("********************************************************************************Send bytes to printer");
			
			dos.flush();
	        dos.close();
	       // inStream.close();
	       // socket.close();
        }
        finally {
        	// close the streams using close method
            try {
                if (inStream != null) {
                	inStream.close();
                }
            } catch (IOException e) {

				e.printStackTrace();
			}
            finally {
            	try {
					socket.close();
				} catch (IOException e) {

					e.printStackTrace();
				}
            } 

        }           
	}
}
