package com.staples.printnpack.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import Util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *     Author: Thomas Schlicher
 *       Date: 20 Feb 2019
 *   
 *   comments: This class will be used to hold methods that will execute CRUD DB Operations on the Carton Data table.
 *   			 
 */

public class CartonDataDAO 
{
	//*******************************
    //SELECT a Carton
    //*******************************
    public static CartonDataTracking searchCartonDataTracking (String barCode) throws SQLException, ClassNotFoundException 
    {
        //Declare a SELECT statement
    	// SoCal DB
        //String selectStmt = "SELECT * FROM PRINTNPACK.dbo.carton_data WHERE Barcode='"+barCode+"'";
        
        // Beloit DB
        String selectStmt = "SELECT * FROM ELI.dbo.carton_data WHERE Barcode='" + barCode + "'";
 
        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = DBUtil.dbExecuteQuery(selectStmt);
 
            //Send ResultSet to the getOrderFromResultSet method and get orders object
            CartonDataTracking carton = getOrderFromResultSet(rsEmp);
 
            //Return employee object
            return carton;
        } catch (SQLException e) {
            System.out.println("While searching an order with " + barCode + " barcode, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    
  //Use ResultSet from DB as parameter and set carton data Object's attributes and return order object.
    private static CartonDataTracking getOrderFromResultSet(ResultSet rs) throws SQLException
    {
    	CartonDataTracking ord = null;
        if (rs.next()) {
        	ord = new CartonDataTracking();
        	ord.setBarcode(rs.getString("Barcode"));
        	ord.setStatus(rs.getInt("status"));
            ord.setTimeScanned(rs.getString("timeScanned"));
            ord.setPrintJobRequestedTime(rs.getString("printJobRequestedTime"));
            ord.setPrintJobStartTime(rs.getString("printJobStartTime"));
            ord.setPrintJobCompleteTime(rs.getString("printJobCompleteTime"));
            ord.setTimePsInsert(rs.getString("timePsInsert"));
            ord.setTimeMmStringRequested("mmStringRequested");
            ord.setTimeMmStringReturn(rs.getString("timeMmStringReturn"));
            ord.setMmStringValue(rs.getString("mmStringValue"));
            ord.setTimeMm1Inserted(rs.getString("timeMm1Inserted"));
            ord.setTimeMm2Inserted(rs.getString("timeMm2Inserted"));
            ord.setTimeMm3Inserted(rs.getString("timeMm3Inserted"));
            ord.setTimeMm4Inserted(rs.getString("timeMm4Inserted"));
            ord.setTimeMm5Inserted(rs.getString("timeMm5Inserted"));
        }
        return ord;
    }
    
    //*******************************
    //SELECT all CartonIds
    //*******************************
    public static ObservableList<CartonDataTracking> searchAllCartonDataTracking () throws SQLException, ClassNotFoundException 
    {
        //Declare a SELECT statement
       /* String selectStmt = "SELECT [Barcode]\r\n" +
        		"		,[status]\r\n" + 
        		"		,[timeScanned]\r\n" + 
        		"		,[printJobRequestedTime]\r\n" + 
        		"		,[printJobStartTime]\r\n" + 
        		"		,[printJobCompleteTime]\r\n" + 
        		"		,[timePsInsert]\r\n" + 
        		"		,[timeMmStringRequest]\r\n" + 
        		"		,[timeMmStringReturn]\r\n" + 
        		"		,[mmStringValue]\r\n" + 
        		"		,[timeMm1Inserted]\r\n" + 
        		"		,[timeMm2Inserted]\r\n" + 
        		"		,[timeMm3Inserted]\r\n" + 
        		"		,[timeMm4Inserted]\r\n" + 
        		"		,[timeMm5Inserted]\r\n" + 
        		"	FROM [PRINTNPACK].[dbo].[carton_data]\r\n" + 
        		"	ORDER BY printJobRequestedTime desc;";*/
        
      //Declare a SELECT statement 
        String selectStmt = "SELECT [Barcode]\r\n" +
        		"		,[status]\r\n" + 
        		"		,[timeScanned]\r\n" + 
        		"		,[printJobRequestedTime]\r\n" + 
        		"		,[printJobStartTime]\r\n" + 
        		"		,[printJobCompleteTime]\r\n" + 
        		"		,[timePsInsert]\r\n" + 
        		"		,[timeMmStringRequest]\r\n" + 
        		"		,[timeMmStringReturn]\r\n" + 
        		"		,[mmStringValue]\r\n" + 
        		"		,[timeMm1Inserted]\r\n" + 
        		"		,[timeMm2Inserted]\r\n" + 
        		"		,[timeMm3Inserted]\r\n" + 
        		"		,[timeMm4Inserted]\r\n" + 
        		"		,[timeMm5Inserted]\r\n" + 
        		"	FROM [ELI].[dbo].[carton_data]\r\n" + 
        		"	ORDER BY printJobRequestedTime desc;";
        
 
        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
        	System.out.println("Executing DB Query: " + selectStmt);
            ResultSet rsOrder = DBUtil.dbExecuteQuery(selectStmt);
 
            //Send ResultSet to the getCartonDataTrackingList method and get carton data object
            ObservableList<CartonDataTracking> orderList = getCartonDataTrackingList(rsOrder);
            //Return employee object
            return orderList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }
    
    //Select * from carton data
    private static ObservableList<CartonDataTracking> getCartonDataTrackingList(ResultSet rs) throws SQLException, ClassNotFoundException 
    {
        //Declare a observable List which comprises of Carton Data Objects
        ObservableList<CartonDataTracking> orderList = FXCollections.observableArrayList();
 
        while (rs.next()) {        	
        	CartonDataTracking carton = new CartonDataTracking();
        	carton.setBarcode(rs.getString("Barcode"));
        	//System.out.println("get barcode id property: " + carton.barcodeProperty());
        	
        	carton.setStatus(rs.getInt("status"));
        	//System.out.println("get status property: " + carton.statusProperty());
        	
        	carton.setTimeScanned(rs.getString("timeScanned"));
        	//System.out.println("get time scanned property: " + carton.timeScannedProperty());
        	
        	carton.setPrintJobRequestedTime(rs.getString("printJobRequestedTime"));
        	//System.out.println("get print job requested time property: " + carton.printJobRequestedTimeProperty());
        	
        	carton.setPrintJobStartTime(rs.getString("printJobStartTime"));
        	//System.out.println("get print job start time property: " + carton.printJobStartTimeProperty());
        	
        	carton.setPrintJobCompleteTime(rs.getString("printJobCompleteTime"));
        	//System.out.println("get print job complete time property: " + carton.printJobCompleteTimeProperty());
        	
        	carton.setTimePsInsert(rs.getString("timePsInsert"));
        	//System.out.println("get time PS insert property: " + carton.timePsInsertProperty());
        	
        	carton.setTimeMmStringRequested(rs.getString("timeMmStringRequest"));
        	//System.out.println("get mm string requested time property: " + carton.timeMmStringRequestedProperty());
        	
        	carton.setTimeMmStringReturn(rs.getString("timeMmStringReturn"));
        	//System.out.println("get mm string returned time property: " + carton.timeMmStringReturnProperty());
        	
        	carton.setMmStringValue(rs.getString("mmStringValue"));
        	//System.out.println("get mm string value property: " + carton.mmStringValueProperty());
        	
        	carton.setTimeMm1Inserted(rs.getString("timeMm1Inserted"));
        	//System.out.println("get time mm 1 inserted property: " + carton.timeMm1InsertedProperty());
        	
        	carton.setTimeMm2Inserted(rs.getString("timeMm2Inserted"));
        	//System.out.println("get time mm 2 inserted property: " + carton.timeMm2InsertedProperty());
        	
        	carton.setTimeMm3Inserted(rs.getString("timeMm3Inserted"));
        	//System.out.println("get time mm 3 inserted property: " + carton.timeMm3InsertedProperty());
        	
        	carton.setTimeMm4Inserted(rs.getString("timeMm4Inserted"));
        	//System.out.println("get time mm 4 inserted property: " + carton.timeMm4InsertedProperty());
        	
        	carton.setTimeMm5Inserted(rs.getString("timeMm5Inserted"));
        	//System.out.println("get time mm 5 inserted property: " + carton.timeMm5InsertedProperty());
        	
            //Add all Carton Data objects to the ObservableList
            orderList.add(carton);
        }
        //return order List (ObservableList of Carton Data)
        return orderList;
    }
    
    //*************************************
    //DELETE a CartonId after reprint
    //*************************************
    public static void deleteOrderWithBarcode (String barCode) throws SQLException, ClassNotFoundException 
    {
        //Declare a DELETE statement
        String updateStmt = "DELETE FROM carton_data WHERE Barcode ="+ barCode;
 
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
    
}
