package com.staples.printnpack;
	
import java.io.IOException;
import com.staples.printnpack.controller.MainWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *     Author: Thomas Schlicher
 *       Date: 2 Feb 2019
 *   
 *   comments: This class will be used to load our FXML file, set our controller class and initialize our UI application.
 *   
 *   		   This application is based on the MVC pattern and our Database uses the DAO pattern.
 *   			 
 */

public class Main extends Application
{
	private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());
		
	private Stage primaryStage;
	
	// Starting JavaFX application
	@Override
	public void start(Stage primaryStage)
	{
		LOGGER.info("Setting our GUI stage and starting application");
		this.primaryStage = primaryStage;		
		mainWindow();	
	}
		
	/**
	 * Comments: setting the main UI stage for JavaFX
	 */
	public void mainWindow()
	{
		try {
			// Loading our FXML file
			FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/MainWindowView.fxml"));
			AnchorPane pane = loader.load();
			
			// Setting our controller class
			MainWindowController mainWindowController = loader.getController();
			mainWindowController.setMain(this);
			
			// Setting the UI scene
			Scene scene = new Scene(pane);
			
			// Setting the stage
			primaryStage.setScene(scene);
			primaryStage.setTitle("Print-n-Pack");
			primaryStage.show();
			
		} catch (IOException e) {
			LOGGER.error("Issue with loading our FXML file: " + e);
			e.printStackTrace();
		}
	}
	
	// Main only calls Launch -- the start() actually starts our application
	public static void main(String[] args)
	{	
		launch(args);
	}
}
