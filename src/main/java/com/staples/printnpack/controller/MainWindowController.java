package com.staples.printnpack.controller;

import java.io.IOException;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.staples.printnpack.Main;
import com.staples.printnpack.PropertiesCache;
import com.staples.printnpack.model.CartonDataDAO;
import com.staples.printnpack.model.CartonDataTracking;
import com.staples.printnpack.model.HospitalLanePrinting;
import com.staples.printnpack.model.OrderTracking;
import com.staples.printnpack.model.OrderTrackingDAO;
import com.staples.printnpack.model.PdfFileNameSearch;
import com.staples.printnpack.model.PdfFileNameSearchDAO;
import com.staples.printnpack.snmp.SnmpManager;

import Util.PrinterConstants;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 *     Author: Thomas Schlicher
 *       Date: 2 Feb 2019
 *   
 *   comments: This class is our MainWindowController (controller) class which will be used with our MainWindowView (View).
 *   		   This class uses multiple background threads which communicate with our business logic (Model), that logic
 *   		   is then pulled into our controller class which sets the returned data to our javaFX application 
 *   		   thread so data can populate on the UI.			 
 */

public class MainWindowController
{
	private static final Logger LOGGER = LogManager.getLogger(MainWindowController.class.getName());
	private Main main;
	
	// Printer 1 Labels
	@FXML private Label printer1StatusColor;
	@FXML private Label printer1Status;
	@FXML private Label printer1StatusMsgBox;
	
	// Printer 2 Status Labels
	@FXML private Label printer2StatusColor;
	@FXML private Label printer2Status;
	@FXML private Label printer2StatusMsgBox;
	
	// Printer 3 Status Labels
	@FXML private Label printer3StatusColor;
	@FXML private Label printer3Status;
	@FXML private Label printer3StatusMsgBox;
		
	// Printer 4 Status Labels
	@FXML private Label printer4StatusColor;
	@FXML private Label printer4Status;
	@FXML private Label printer4StatusMsgBox;
	
	// Printer 5 Status Labels
	@FXML private Label printer5StatusColor;
	@FXML private Label printer5Status;
	@FXML private Label printer5StatusMsgBox;
	
	// Reject Lane Printer Status Labels
	@FXML private Label rejectLaneStatusColor;
	@FXML private Label rejectLaneStatus;
	@FXML private Label rejectLaneStatusMsgBox;
	
	// Seacrh Table Text Field
	@FXML private TextField cartonIdSearchField;
	
	// Search / Re-Print Button
	//@FXML private Button searchButton;
	//@FXML private Button reprintButton;
	//@FXML private Button clearButton;
	@FXML private Label msgLabel;
	
	// Printer Status Table View
	@FXML private TableColumn<OrderTracking, String>  printerName;
	@FXML private TableColumn<OrderTracking, String>  pStatusCode;
	@FXML private TableColumn<OrderTracking, String>  pStatusColor;
	@FXML private TableColumn<OrderTracking, String>  pAvailability;
	@FXML private TableColumn<OrderTracking, String>  pMsg;
	
	// Carton Data Table View
	@FXML private TableView<CartonDataTracking> cartonDataTable;
	@FXML private TableColumn<CartonDataTracking, String>  cartonId;
	@FXML private TableColumn<CartonDataTracking, Integer>  status;
	@FXML private TableColumn<CartonDataTracking, String>  timeScanned;
	@FXML private TableColumn<CartonDataTracking, String>  printJobRequestedTime;
	@FXML private TableColumn<CartonDataTracking, String>  printJobStartTime;
	@FXML private TableColumn<CartonDataTracking, String>  printJobCompleteTime;
	@FXML private TableColumn<CartonDataTracking, String>  psInsertTime;
	@FXML private TableColumn<CartonDataTracking, String>  timeMmStringRequest;
	@FXML private TableColumn<CartonDataTracking, String>  mmStringReturnTime;
	@FXML private TableColumn<CartonDataTracking, String>  mmStringValue;
	@FXML private TableColumn<CartonDataTracking, String>  timeMm1Inserted;
	@FXML private TableColumn<CartonDataTracking, String>  timeMm2Inserted;
	@FXML private TableColumn<CartonDataTracking, String>  timeMm3Inserted;
	@FXML private TableColumn<CartonDataTracking, String>  timeMm4Inserted;
	@FXML private TableColumn<CartonDataTracking, String>  timeMm5Inserted;
	
	// Wes barcode data
	@FXML private TableView<PdfFileNameSearch> wesDataTable;
	@FXML private TableColumn<CartonDataTracking, String>  wesBarcode;
	
	
	// Search / Re-Print Table View
	//@FXML private TableView<CartonDataTracking> searchReprintTable;	
	//@FXML private TableColumn<CartonDataTracking, String>  searchTableCartonId;
	
	// WES sites
	@FXML private TableView<PdfFileNameSearch> searchReprintTable;
	@FXML private TableColumn<PdfFileNameSearch, String>  searchTableCartonId;
	
	// Creating a scheduled executor to run a thread at given intervals
	private ScheduledExecutorService executor;
	
	// Setting Main - start of the application so it knows our controller class
	public void setMain(Main main)
	{
		this.main = main;
	}
	
	/**
	 * The initialize method will automatically be invoked by FXML when the application gets loaded.
	 * 
	 */
	@FXML
	private void initialize()
	{
		LOGGER.info("Creating a scheduled executor to handle 3 threads");
		// create executor that uses daemon threads;
		executor = Executors.newScheduledThreadPool(4, runnable -> {
            Thread t = new Thread(runnable);
            t.setDaemon(true);
            return t;
        });
		
		// Starting a background thread that handles getting the printer status 
		// and writing that data to database
		startSnmpCommThread();
		
		// Starting background thread that will pull data from database and update
		// UI at certain intervals
		fillOrdersTable();	
		
		// Starting a background thread that handles getting the Carton Data and populating the UI
		fillCartonDataTableView();
	}
	
	/********************************************************************************************************************************************
	 * This section works with the print n pack database retrieving data from carton_data table
	 * This section will also handle keeping the TableView on the UI in sync with the database data
	 ********************************************************************************************************************************************/
	// Grab all carton data from database
	@FXML
	private void searchAllCartonData()
	{
		LOGGER.info("Searching all carton_data in DB");
		try {
			ObservableList<CartonDataTracking> cartonData = CartonDataDAO.searchAllCartonDataTracking();
			populateAllCartonData(cartonData);
			
		} catch (ClassNotFoundException e) {
			LOGGER.error("Class not found error: " + e);
			e.printStackTrace();
		} catch (SQLException e) {
			LOGGER.error("Error occured while getting information from Print-n-Pack DB and Table carton_data: " + e);
			e.printStackTrace();
		}
	}
	
	// Populate Carton Data for TableView
	@FXML
	private void populateAllCartonData(ObservableList<CartonDataTracking> cartonData)
	{
		cartonDataTable.setItems(cartonData);
	}
	
	//@FXML
	private void fillCartonDataTableView()
	{
		LOGGER.info("Starting background thread for fillCartonDataTableView()");
		// Starting background task that doesn't return any data
		Task<Void> dbReadCartonDataTask = new Task<Void>(){
			// Call method is a standard method that goes with the Task background thread (must have call() method)
            @Override
            protected Void call() throws ClassNotFoundException, SQLException{
            	// Continually gather data from DB at a given interval
            	while(true)
            	{
            		searchAllCartonData();
            		
            		// Platform.runLater method is the javaFX UI thread and must be called to populate data on the UI
            		// This is not a background thread but, when this is called it pulls the data to the UI thread.
            		Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                        	cartonId.setCellValueFactory(cellData -> cellData.getValue().barcodeProperty());
                        	//status.setCellValueFactory(cellData -> cellData.getValue().statusProperty().asObject());
                        	//timeScanned.setCellValueFactory(cellData -> cellData.getValue().timeScannedProperty());
                        	printJobRequestedTime.setCellValueFactory(cellData -> cellData.getValue().printJobRequestedTimeProperty());
                        	//printJobStartTime.setCellValueFactory(cellData -> cellData.getValue().printJobStartTimeProperty());
                        	//printJobCompleteTime.setCellValueFactory(cellData -> cellData.getValue().printJobCompleteTimeProperty());
                        	//psInsertTime.setCellValueFactory(cellData -> cellData.getValue().timePsInsertProperty());
                        	//timeMmStringRequest.setCellValueFactory(cellData -> cellData.getValue().timeMmStringRequestedProperty());
                        	//mmStringReturnTime.setCellValueFactory(cellData -> cellData.getValue().timeMmStringReturnProperty());
                        	//mmStringValue.setCellValueFactory(cellData -> cellData.getValue().mmStringValueProperty());
                        	//timeMm1Inserted.setCellValueFactory(cellData -> cellData.getValue().timeMm1InsertedProperty());
                        	//timeMm2Inserted.setCellValueFactory(cellData -> cellData.getValue().timeMm2InsertedProperty());
                        	//timeMm3Inserted.setCellValueFactory(cellData -> cellData.getValue().timeMm3InsertedProperty());
                        	//timeMm4Inserted.setCellValueFactory(cellData -> cellData.getValue().timeMm4InsertedProperty());
                        	//timeMm5Inserted.setCellValueFactory(cellData -> cellData.getValue().timeMm5InsertedProperty());
                        }
                      });
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						LOGGER.info("Interruped Exception " + e);
						e.printStackTrace();
					}	
            }                 
          }            	
	   };
	   // Executor cycles this thread every 400 Milliseconds
	   executor.scheduleAtFixedRate(dbReadCartonDataTask, 0, 3000, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Comments: This method will be used to start a background thread that will get all the data 
	 * 		     from the orders table in the database and populate that data in our UI status table.
	 */
	@FXML
	private void fillOrdersTable()
	{
		LOGGER.info("Starting background thread for fillOrdersTable()");
		// Starting background task that doesn't return any data
		Task<Void> dbReadTask = new Task<Void>(){
			// Call method is a standard method that goes with the Task background thread (must have call() method)
            @Override
            protected Void call() throws ClassNotFoundException, SQLException{
            	// Continually gather data from DB and a given interval
            	while(true)
            	{
            		ObservableList<OrderTracking> list = OrderTrackingDAO.searchAllOrderTracking();
            		LOGGER.info("Class: MainWindowController - Setting data on javaFX UI thread for the printer status table");
            		
            		// Platform.runLater method is the javaFX UI thread and must be called to populate data on the UI
            		// This is not a background thread but, when this is called it pulls the data to the UI thread.
            		Platform.runLater(new Runnable() {
                        @Override
                        public void run() {

                        	// Call loopThroughTableData which will display data on UI
							loopThroughTabledata(list);
                        }
                      });
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						LOGGER.info("Class: MainWindowController - Interruped Exception " + e);
						e.printStackTrace();
					}	
            }                 
          }            	
	   };
	   // Executor cycles this thread every 400 Milliseconds
	   executor.scheduleAtFixedRate(dbReadTask, 0, 200, TimeUnit.MILLISECONDS);
	}
	
	/********************************************************************************************************************************************
	 * This section will start a background thread that will continuously run and check all the printers to make
	 * sure they are in a good running condition.
	 ********************************************************************************************************************************************/
	/**
	 * Comments: Starting a background thread that will continually retrieve the printer status for each printer
	 * 			 and then populate the database with any updates.
	 */
	private void startSnmpCommThread() 
	{
		// Starting background thread
	    Task<Void> task1 = new Task<Void>() 
	    {
	    	// Must have call() for our Task (thread)
	        @Override
	        protected Void call() throws Exception 
	        {
	        	// Continually loop through the printer, get status and populate database
	        	while(true) {
	        		SnmpManager smp = new SnmpManager();
					smp.initializeSnmpClient();
					Thread.sleep(1000);
	        	}
	        }
	    };
	    executor.scheduleAtFixedRate(task1, 0, 200, TimeUnit.MILLISECONDS);
	}

	// Search carton Id by pressing "Enter" on the text field
	@FXML
	public void onEnter(ActionEvent event) 
	{
		clearFields();
		final String searchStatus = "Searching Database for Carton Id";
    	
		//cartonIdDatabaseSearch.isRunning()
    	if (cartonIdDatabaseSearch.isRunning()) 
    	{
    		// Do nothing as search is in progress

    	} else {
    		
    		updateMsgLabelReprintTable(searchStatus);
    		cartonIdDatabaseSearch.restart();
    		//printintPdfOnly.restart();
    		
    	}
	}   

    // Background thread that searches both WES an Print-n-Pack databases for a cartonId / barcode and then re-prints the packing slip
	// For greencastle, do not do a database look up on barcode.
    Service<Void> cartonIdDatabaseSearch = new Service<Void>() {

		@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {

				@Override
				protected Void call() throws Exception 
				{	
					//cartonIdSearchField.setDisable(true);
					String getBarcodeText = cartonIdSearchField.getText().trim();
					
					cartonIdSearchField.setText("");
					//cartonIdSearchField.setC
		        	
		        	// NON Wes sites barcode look up
					//CartonDataTracking carton = CartonDataDAO.searchCartonDataTracking(getBarcodeText);
		        	
		        	// Wes sites barcode look up
					PdfFileNameSearch wesBarcodeSearch = PdfFileNameSearchDAO.searchWesCartonDataTracking(getBarcodeText);
					
					Platform.runLater(() -> {
						// Used to show status of database searching
						final String databaseError = "Problem while searching for cartonId in database";
						final String dbSearchSuccessful = "Carton Id was successfully retrieved from Database";
						final String printingError = "Printing did not work, make sure printer is on";
						final String dbSeacrhNull = "Carton Id is null, please try another search";
						
						
						try {
				     							
							//String cartonStatus = carton.getBarcode().toString();
							//System.out.println("Barcode status = " + carton);
							
							if(!(wesBarcodeSearch == (null)))
							{
								//Populate Carton Id on TableView and Display on TextArea
								//populateCarton(carton);
								
								// Wes barcode populate in UI table
					        	populateWesCarton(wesBarcodeSearch);
					        	
								updateMsgLabelReprintTable(dbSearchSuccessful);
								//msgLabel.setText(dbSearchSuccessful);
								
								HospitalLanePrinting hlp = new HospitalLanePrinting();
						 	 	
						 	 	// if / else statement used here to allow updating of the message label on UI in regards to
						 	 	// search / reprint table.
								if(hlp.searchPdfAndPrint(getBarcodeText))
								{
											
									System.out.println("**************************************File was found " + getBarcodeText);
									updateMsgLabelReprintTable("File was found for carton: " + getBarcodeText);
																					 	
								}else {
											
									System.out.println("File was not found");
									updateMsgLabelReprintTable("File was not found for carton: " + getBarcodeText);
										 	
								}	
										
							}else {
								updateMsgLabelReprintTable(dbSeacrhNull);
							}	
							
							cartonIdSearchField.setText("");
							//cartonIdSearchField.setDisable(false);
							
									
				        } catch (ClassNotFoundException e) {
				        	
				        	updateMsgLabelReprintTable(databaseError);
							e.printStackTrace();
							LOGGER.error("Problem while searching for cartonId");
							
				       } catch (SocketException e) {
				    	   updateMsgLabelReprintTable("Error connecting to FTP / Printer");
				    	   LOGGER.info("SocketException occured : " + e);
							e.printStackTrace();
						} catch (IOException e) {
							updateMsgLabelReprintTable("Error reading from PDF file");
							LOGGER.info("IOException occured : " + e);
							e.printStackTrace();
					    }finally {
					    	
					    	cartonIdSearchField.setText("");
							//cartonIdSearchField.setDisable(false);
					    }
					});
					return null;	
				}	
			};	
		}	
    };
    
    /**
     * Comments: This method is used for green castle site only. When the user searches for a specific barcode the application will
     * 			 no longer look in the database to verify barcode, instead we will look directly for PDF and print it. If PDF is 
     * 			 not available then user will be notified.
     */
    Service<Void> printintPdfOnly = new Service<Void>() {

    	@Override
		protected Task<Void> createTask() {
			return new Task<Void>() {

				@Override
				protected Void call() throws Exception {
					cartonIdSearchField.setDisable(true);
					String getBarcodeText = cartonIdSearchField.getText().trim();
					
					HospitalLanePrinting hlp2 = new HospitalLanePrinting();
					updateMsgLabelReprintTable("Printing packingslip for carton: " + getBarcodeText);
			 	 	
			 	 	// if / else statement used here to allow updating of the message label on UI in regards to
			 	 	// search / reprint table.

						try {
							if(hlp2.searchPdfAndPrint(getBarcodeText))
							{
								
								System.out.println("File was found");
								updateMsgLabelReprintTable("Print Successful!");
								
							}else {
								
								System.out.println("File was not found");
								updateMsgLabelReprintTable("Printing was Unsuccessful");
							 	
							}
						} catch (IOException e) {
							updateMsgLabelReprintTable("Printer is Unavailable");
							e.printStackTrace();
						} catch (Exception e) {
							updateMsgLabelReprintTable("Printer is Unavailable");
							e.printStackTrace();
						} finally {
							cartonIdSearchField.setText("");
							cartonIdSearchField.setDisable(false);
						}
					
					
			 	 	cartonIdSearchField.setText("");
					cartonIdSearchField.setDisable(false);
					return null;
				}
				
			};
    	}
    };
    
    // Populate carton Id for WES sites on the table view in Print-n-Pack UI
    @FXML
    private void populateWesCarton (PdfFileNameSearch pdf) throws ClassNotFoundException 
    {
    	Platform.runLater(() -> {
    		
    			//Declare and ObservableList for table view
                ObservableList<PdfFileNameSearch> WesCartonData = FXCollections.observableArrayList();
                
                //Add cartonId to the ObservableList
                WesCartonData.add(pdf);
                System.out.println("cdt data : " + pdf);
                
                
                //Set items to the searchReprintTable
                searchReprintTable.setItems(WesCartonData);
                searchTableCartonId.setCellValueFactory(cellData -> cellData.getValue().barcodeProperty()); 
                
        });
    }
    
    //Populate carton Id for NON WES sites on the table view in Print-n-Pack UI
   /* @FXML
    private void populateCarton (CartonDataTracking cdt) throws ClassNotFoundException 
    {
    	Platform.runLater(() -> {
    			//Declare and ObservableList for table view
                ObservableList<CartonDataTracking> cartonData = FXCollections.observableArrayList();
                
                //Add cartonId to the ObservableList
                cartonData.add(cdt);
                System.out.println("cdt data : " + cdt);
                
                
                //Set items to the searchReprintTable
                searchReprintTable.setItems(cartonData);
                searchTableCartonId.setCellValueFactory(cellData -> cellData.getValue().barcodeProperty()); 	
        });
    }*/
    
   
    
    // Clear search criteria
    @FXML
    private void clearFields()
    {
    	Platform.runLater(() -> {
    		cartonIdSearchField.setDisable(false);
    		searchReprintTable.getItems().clear();
    	});
    }
    
    // Updates status message based on status of the reprint table i.e printing, searching, any errors
    @FXML
    private void updateMsgLabelReprintTable(String myMsg) 
    {
    	Platform.runLater(() -> {
        	msgLabel.setText("");
        	msgLabel.setText(myMsg);	
    	});
    }
    
    // Clears the msglabel shown in UI
    @FXML
    private void clearMsgLabel() 
    {
    	Platform.runLater(() -> {
        	msgLabel.setText("");	
    	});
    }
    
    /**
	 * Comments: This method will handle setting all the UI Labels based on the printer and its status
	 */
	@FXML
	public void loopThroughTabledata(ObservableList<OrderTracking> aList)
	{
		final String ready = PropertiesCache.getInstance().getProperty("ready");
		final String warning = PropertiesCache.getInstance().getProperty("warning");
		final String down = PropertiesCache.getInstance().getProperty("down");
		final String readyText = PropertiesCache.getInstance().getProperty("ready.text");
		final String warningText = PropertiesCache.getInstance().getProperty("warning.text");
		final String downText = PropertiesCache.getInstance().getProperty("down.text");
		final String setFxPrinterReadyColor = PropertiesCache.getInstance().getProperty("fx.ready.color");
		final String setFxPrinterWarningColor = PropertiesCache.getInstance().getProperty("fx.warning.color");
		final String setFxPrinterDownColor = PropertiesCache.getInstance().getProperty("fx.down.color");

		// Printers change for each site
		final String printer1 = PrinterConstants.CHARLOTTE_PRINTER1;
		final String printer2 = PrinterConstants.CHARLOTTE_PRINTER2;
		final String printer3 = PrinterConstants.CHARLOTTE_PRINTER3;
		final String printer4 = PrinterConstants.CHARLOTTE_PRINTER4;
		final String printer5 = PrinterConstants.CHARLOTTE_PRINTER5;
		final String hospitalPrinter = PrinterConstants.CHARLOTTE_HOSPITAL_PRINTER_LINE1;
	
		for(OrderTracking ot : aList)
		{
			String whichPrinter = ot.getPrinterId();
			switch(whichPrinter)
			{
				// Printer 1
				case printer1:
					String p1 =  ot.getStatus();
				
					if(p1.equalsIgnoreCase(ready)) 
					{
						printer1StatusColor.setText(readyText);
						printer1StatusColor.setStyle(setFxPrinterReadyColor);
		
					}else if(p1.equalsIgnoreCase(warning))
					{
						printer1StatusColor.setText(warningText);
						printer1StatusColor.setStyle(setFxPrinterWarningColor);
		
					}else if(p1.equalsIgnoreCase(down)) 
					{
						printer1StatusColor.setText(downText);
						printer1StatusColor.setStyle(setFxPrinterDownColor);
					}	
					printer1Status.setText(ot.getPrinterAvailability());
					printer1StatusMsgBox.setText(ot.getPrinterMessage());
					break;
					
				// Printer 2
				case printer2:
					String p2 =  ot.getStatus();
					if(p2.equalsIgnoreCase(ready)) 
					{
						printer2StatusColor.setText(readyText);
						printer2StatusColor.setStyle(setFxPrinterReadyColor);

					}else if(p2.equalsIgnoreCase(warning))
					{
						printer2StatusColor.setText(warningText);
						printer2StatusColor.setStyle(setFxPrinterWarningColor);

					}else if(p2.equalsIgnoreCase(down)) 
					{
						printer2StatusColor.setText(downText);
						printer2StatusColor.setStyle(setFxPrinterDownColor);	
					}
					printer2Status.setText(ot.getPrinterAvailability());
					printer2StatusMsgBox.setText(ot.getPrinterMessage());
					break;
				
				// Printer 3
				case printer3:
					String p3 =  ot.getStatus();
					
					if(p3.equalsIgnoreCase(ready)) 
					{
						printer3StatusColor.setText(readyText);
						printer3StatusColor.setStyle(setFxPrinterReadyColor);
		
					}else if(p3.equalsIgnoreCase(warning))
					{
						printer3StatusColor.setText(warningText);
						printer3StatusColor.setStyle(setFxPrinterWarningColor);
		
					}else if(p3.equalsIgnoreCase(down)) 
					{
						printer3StatusColor.setText(downText);
						printer3StatusColor.setStyle(setFxPrinterDownColor);	
					}	
					printer3Status.setText(ot.getPrinterAvailability());
					printer3StatusMsgBox.setText(ot.getPrinterMessage());
					break;
					
				// Printer 4
				case printer4:
					String p4 =  ot.getStatus();
					
					if(p4.equalsIgnoreCase(ready)) 
					{
						printer4StatusColor.setText(readyText);
						printer4StatusColor.setStyle(setFxPrinterReadyColor);
		
					}else if(p4.equalsIgnoreCase(warning))
					{
						printer4StatusColor.setText(warningText);
						printer4StatusColor.setStyle(setFxPrinterWarningColor);
		
					}else if(p4.equalsIgnoreCase(down)) 
					{
						printer4StatusColor.setText(downText);
						printer4StatusColor.setStyle(setFxPrinterDownColor);	
					}	
					printer4Status.setText(ot.getPrinterAvailability());
					printer4StatusMsgBox.setText(ot.getPrinterMessage());
					break;
					
				// Printer 5	
				case printer5:
					String p5 =  ot.getStatus();
					
					if(p5.equalsIgnoreCase(ready)) 
					{
						printer5StatusColor.setText(readyText);
						printer5StatusColor.setStyle(setFxPrinterReadyColor);
		
					}else if(p5.equalsIgnoreCase(warning))
					{
						printer5StatusColor.setText(warningText);
						printer5StatusColor.setStyle(setFxPrinterWarningColor);
		
					}else if(p5.equalsIgnoreCase(down)) 
					{
						printer5StatusColor.setText(downText);
						printer5StatusColor.setStyle(setFxPrinterDownColor);	
					}	
					printer5Status.setText(ot.getPrinterAvailability());
					printer5StatusMsgBox.setText(ot.getPrinterMessage());
					break;
				
				// Hospital Lane Printer	
				case hospitalPrinter:
					String hp =  ot.getStatus();
					
					if(hp.equalsIgnoreCase(ready)) 
					{
						rejectLaneStatusColor.setText(readyText);
						rejectLaneStatusColor.setStyle(setFxPrinterReadyColor);
		
					}else if(hp.equalsIgnoreCase(warning))
					{
						rejectLaneStatusColor.setText(warningText);
						rejectLaneStatusColor.setStyle(setFxPrinterWarningColor);
		
					}else if(hp.equalsIgnoreCase(down)) 
					{
						rejectLaneStatusColor.setText(downText);
						rejectLaneStatusColor.setStyle(setFxPrinterDownColor);	
					}	
					rejectLaneStatus.setText(ot.getPrinterAvailability());
					rejectLaneStatusMsgBox.setText(ot.getPrinterMessage());
					break;
			}
		}
	}
    
}
