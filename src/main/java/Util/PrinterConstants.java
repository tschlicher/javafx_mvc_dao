package Util;

/**
 *     Author: Thomas Schlicher
 *       Date: 4/8/2019
 *   
 *   comments: 
 *   			 
 */
public final class PrinterConstants 
{
	private PrinterConstants() {}

	// SoCal Printer IPs
	public static final String SOCAL_PRINTER1 = "10.30.0.34";
	public static final String SOCAL_PRINTER2 = "10.30.0.35";
	public static final String SOCAL_PRINTER3 = "10.30.0.36";
	public static final String SOCAL_PRINTER4 = "10.30.0.37";
	public static final String SOCAL_PRINTER5 = "10.30.0.38";
	public static final String SOCAL_HOSPITAL_PRINTER = "10.30.0.126";
	
	// Beloit Printer IPs - Print-n-Pack PLC Line 1
	public static final String BEL_PRINTER1_LINE1 = "10.31.160.60";
	public static final String BEL_PRINTER2_LINE1 = "10.31.160.61";
	public static final String BEL_PRINTER3_LINE1 = "10.31.160.62";
	public static final String BEL_PRINTER4_LINE1 = "10.31.160.63";
	public static final String BEL_PRINTER5_LINE1 = "10.31.160.64";
	public static final String BEL_HOSPITAL_PRINTER_LINE1 = "10.29.12.115";
	public static final String BEL_HOSPITAL_PRINTER2_LINE1 = "10.29.12.116";
	
	// Beloit Printer IPs - Print-n-Pack PLC Line 1
	public static final String BEL_PRINTER1_LINE2 = "10.31.160.65";
	public static final String BEL_PRINTER2_LINE2 = "10.31.160.66";
	public static final String BEL_PRINTER3_LINE2 = "10.31.160.67";
	public static final String BEL_PRINTER4_LINE2 = "10.31.160.68";
	public static final String BEL_PRINTER5_LINE2 = "10.31.160.69";
	public static final String BEL_HOSPITAL_PRINTER_LINE2 = "10.29.12.117";
	public static final String BEL_HOSPITAL_PRINTER2_LINE2 = "10.29.12.123";
	
	// Green Castle Printer IPs - Print-n-Pack PLC Line 1
	public static final String GC_PRINTER1_LINE1 = "10.30.238.60";
	public static final String GC_PRINTER2_LINE1 = "10.30.238.61";
	public static final String GC_PRINTER3_LINE1 = "10.30.238.62";
	public static final String GC_PRINTER4_LINE1 = "10.30.238.63";
	public static final String GC_PRINTER5_LINE1 = "10.30.238.64";
	public static final String GC_HOSPITAL_PRINTER_LINE1 = "10.30.224.148";
	
	// Green Castle Printer IPs - Print-n-Pack PLC Line 2
	public static final String GC_PRINTER1_LINE2 = "10.30.238.65";
	public static final String GC_PRINTER2_LINE2 = "10.30.238.66";
	public static final String GC_PRINTER3_LINE2 = "10.30.238.67";
	public static final String GC_PRINTER4_LINE2 = "10.30.238.68";
	public static final String GC_PRINTER5_LINE2 = "10.30.238.69";
	public static final String GC_HOSPITAL_PRINTER_LINE2 = "10.30.224.149";
	
	// Dallas Printer IPs - Print-n-Pack PLC Line 1
	public static final String DALLAS_PRINTER1 = "10.31.162.60";
	public static final String DALLAS_PRINTER2 = "10.31.162.61";
	public static final String DALLAS_PRINTER3 = "10.31.162.62";
	public static final String DALLAS_PRINTER4 = "10.31.162.63";
	public static final String DALLAS_PRINTER5 = "10.31.162.64";
	public static final String DALLAS_HOSPITAL_PRINTER = "10.31.76.185";
		
	/*// Green Castle Printer IPs - Print-n-Pack PLC Line 2
	public static final String GC_PRINTER1_LINE2 = "10.30.238.65";
	public static final String GC_PRINTER2_LINE2 = "10.30.238.66";
	public static final String GC_PRINTER3_LINE2 = "10.30.238.67";
	public static final String GC_PRINTER4_LINE2 = "10.30.238.68";
	public static final String GC_PRINTER5_LINE2 = "10.30.238.69";
	public static final String GC_HOSPITAL_PRINTER_LINE2 = "10.30.224.149";*/
	
	// Lithia Printer IPs - Print-n-Pack PLC Line 1
	public static final String LITHIA_PRINTER1_LINE1 = "10.31.161.60";
	public static final String LITHIA_PRINTER2_LINE1 = "10.31.161.61";
	public static final String LITHIA_PRINTER3_LINE1 = "10.31.161.62";
	public static final String LITHIA_PRINTER4_LINE1 = "10.31.161.63";
	public static final String LITHIA_PRINTER5_LINE1 = "10.31.161.64";
	public static final String LITHIA_HOSPITAL_PRINTER_LINE1 = "10.31.224.130";
		
	// Lithia Printer IPs - Print-n-Pack PLC Line 2
	public static final String LITHIA_PRINTER1_LINE2 = "10.31.161.65";
	public static final String LITHIA_PRINTER2_LINE2 = "10.31.161.66";
	public static final String LITHIA_PRINTER3_LINE2 = "10.31.161.67";
	public static final String LITHIA_PRINTER4_LINE2 = "10.31.161.68";
	public static final String LITHIA_PRINTER5_LINE2 = "10.31.161.69";
	public static final String LITHIA_HOSPITAL_PRINTER_LINE2 = "10.31.224.134";
	
	// Charlotte Printer IPs - Print-n-Pack PLC Line 2
	public static final String CHARLOTTE_PRINTER1 = "10.30.218.60";
	public static final String CHARLOTTE_PRINTER2 = "10.30.218.61";
	public static final String CHARLOTTE_PRINTER3 = "10.30.218.62";
	public static final String CHARLOTTE_PRINTER4 = "10.30.218.63";
	public static final String CHARLOTTE_PRINTER5 = "10.30.218.64";
	public static final String CHARLOTTE_HOSPITAL_PRINTER_LINE1 = "10.30.208.171";
}
