package com.staples.printnpack.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *     Author: Thomas Schlicher
 *       Date: 2 May 2019
 *   
 *   comments: This class will be used to hold information about carton data and packing slip data. It contains get / set methods
 *   		   and properties for all fields of this model class. The property field let's us know when information 
 *   		   has changed and this will help keep the data in sync with our view.
 *   			 
 */

public class PdfFileNameSearch 
{
	private StringProperty FileName;
	private StringProperty Barcode;
	
	public PdfFileNameSearch()
	{
		this.FileName = new SimpleStringProperty();
		this.Barcode = new SimpleStringProperty();
	}
	
	// Barcode
	public void setBarcode(String barCode)
	{
		this.Barcode.set(barCode);
	}
		
	public String getBarcode() 
	{
	    return barcodeProperty().get();
	}
	    
	public StringProperty barcodeProperty()
	{
	    return Barcode;
	}
	
	// File names
	public void setFileName(String fName)
	{
		this.FileName.set(fName);
	}
		
	public String getFileName() 
	{
	    return fileNameProperty().get();
	}
	    
	public StringProperty fileNameProperty()
	{
	    return FileName;
	}
	
}
