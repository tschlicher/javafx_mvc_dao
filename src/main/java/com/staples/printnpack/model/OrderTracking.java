package com.staples.printnpack.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *     Author: Thomas Schlicher
 *       Date: 12 Feb 2019
 *   
 *   comments: This class will be used to hold information about printer statuses. It contains get / set methods
 *   		   and properties for all fields of this model class. The property field let's us know when information 
 *   		   has changed and this will help keep the data in sync with our view.
 *   			 
 */

public class OrderTracking 
{
	private StringProperty printerId;
	private StringProperty printerStatus;
	private StringProperty printerStatusColor;
	private StringProperty printerAvailability;
	private StringProperty printerMessage;
	
	public OrderTracking()
	{
		this.printerId = new SimpleStringProperty();
		this.printerStatus = new SimpleStringProperty();
		this.printerStatusColor = new SimpleStringProperty();
		this.printerAvailability = new SimpleStringProperty();
		this.printerMessage = new SimpleStringProperty();
	}
	
	// PrinterId
	public void setPrinterId(String barCode)
	{
        this.printerId.set(barCode);
    }
	
    public String getPrinterId() 
    {
        return printerIdProperty().get();
    }
    
    public StringProperty printerIdProperty()
    {
        return printerId;
    }
    
    // Status
    public void setStatus(String status)
    {
        this.printerStatus.set(status);
    }
    
    public String getStatus() 
    {
        return statusProperty().get();
    }
 
    public StringProperty statusProperty() 
    {
        return printerStatus;
    }
    
    // Status Color
    public void setStatusColor(String statusColor)
    {
        this.printerStatusColor.set(statusColor);
    }
    
    public String getStatusColor() 
    {
        return statusColorProperty().get();
    }
 
    public StringProperty statusColorProperty() 
    {
        return printerStatusColor;
    }
    
    // Availability
    public void setPrinterAvailability(String statusColor)
    {
        this.printerAvailability.set(statusColor);
    }
    
    public String getPrinterAvailability() 
    {
        return printerAvailabilityProperty().get();
    }
 
    public StringProperty printerAvailabilityProperty() 
    {
        return printerAvailability;
    }
    
    // Message
    public void setPrinterMessage(String statusColor)
    {
        this.printerMessage.set(statusColor);
    }
    
    public String getPrinterMessage() 
    {
        return printerMessageProperty().get();
    }
 
    public StringProperty printerMessageProperty() 
    {
        return printerMessage;
    }
}
