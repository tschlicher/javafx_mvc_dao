package com.staples.printnpack.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HandlePrinterStatus
{
	private static final Logger LOGGER = LogManager.getLogger(HandlePrinterStatus.class.getName());
	
	/**
	 * Comments: Method used to get the printer status as an int and return 2 messages, one message will be used
	 * 			 in the UI label and the more in-depth message will be used in the UI textArea.
	 * 
	 * @param statusCode
	 * @return
	 */
	public synchronized String[] isPrinterAvailable(String statusCode)
	{	
		int intStatusCode = Integer.parseInt(statusCode);
		// Used to store detailed messages of printer status
		String allPrinterStatus = null;
		
		// Status that will be used for the printer status label
		final String isAvailable = "Available";
		final String lowToner = "Low Toner";
		final String printerisDown = "Down";
		
		// Printer is Available
		final int sCode2 = 2;
		
		// Printer is available, but has low toner
		final int sCode3 = 3;
		
		// noPaper/noToner/doorOpen/jammed/offline/seiviceRequested/Self test
		final int sCode5 = 5;
		
		// Setting string array to return 2 messages corresponding to printer status
		String[] arr2 = new String[3];
		
		String[] nullArr = {"Unkown Status", "Unkown Status", "Unknown Status"};
		
		if(intStatusCode == sCode2)
		{
			LOGGER.info("Class: handlePrinterStatus - Printer is available");
			allPrinterStatus = "Printer is in an operational state";
			arr2[0] = "GREEN";
			arr2[1] = isAvailable;
			arr2[2] = allPrinterStatus;
			return arr2;
		}
		else if(intStatusCode == sCode3){
			LOGGER.info("Class: handlePrinterStatus - Printer is available, but has low toner");
			allPrinterStatus = "Warning!! Printer is available, but has low toner";	
			arr2[0] = "YELLOW";
			arr2[1] = lowToner;
			arr2[2] = allPrinterStatus;
			return arr2;
		}
		else if(intStatusCode == sCode5){
			LOGGER.info("Class: handlePrinterStatus - Printer is NOT Available");
			allPrinterStatus = "Printer Down - noPaper/noToner/doorOpen/jammed/offline/seiviceRequested/Self test";
			arr2[0] = "RED";
			arr2[1] = printerisDown;
			arr2[2] = allPrinterStatus;
			return arr2;
		}
		return nullArr;		
	}
}
