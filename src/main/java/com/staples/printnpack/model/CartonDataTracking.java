package com.staples.printnpack.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *     Author: Thomas Schlicher
 *       Date: 20 Feb 2019
 *   
 *   comments: This class will be used to hold information about carton data. It contains get / set methods
 *   		   and properties for all fields of this model class. The property field let's us know when information 
 *   		   has changed and this will help keep the data in sync with our view.
 *   			 
 */

public class CartonDataTracking 
{
	private StringProperty Barcode;
	private IntegerProperty status;
	private StringProperty timeScanned;
	private StringProperty printJobRequestedTime;
	private StringProperty printJobStartTime;
	private StringProperty printJobCompleteTime;
	private StringProperty timePsInsert;
	private StringProperty timeMmStringRequest;
	private StringProperty timeMmStringReturn;
	private StringProperty mmStringValue;
	private StringProperty timeMm1Inserted;
	private StringProperty timeMm2Inserted;
	private StringProperty timeMm3Inserted;
	private StringProperty timeMm4Inserted;
	private StringProperty timeMm5Inserted;
	
	public CartonDataTracking()
	{
		this.Barcode = new SimpleStringProperty();
		this.status = new SimpleIntegerProperty();
		this.timeScanned = new SimpleStringProperty();
		this.printJobRequestedTime = new SimpleStringProperty();
		this.printJobStartTime = new SimpleStringProperty();
		this.printJobCompleteTime = new SimpleStringProperty();
		this.timePsInsert = new SimpleStringProperty();
		this.timeMmStringRequest = new SimpleStringProperty();
		this.timeMmStringReturn = new SimpleStringProperty();
		this.mmStringValue = new SimpleStringProperty();
		this.timeMm1Inserted = new SimpleStringProperty();
		this.timeMm2Inserted = new SimpleStringProperty();
		this.timeMm3Inserted = new SimpleStringProperty();
		this.timeMm4Inserted = new SimpleStringProperty();
		this.timeMm5Inserted = new SimpleStringProperty();
	}
	
	// Barcode
	public void setBarcode(String barCode)
	{
        this.Barcode.set(barCode);
    }
	
    public String getBarcode() 
    {
        return barcodeProperty().get();
    }
    
    public StringProperty barcodeProperty()
    {
        return Barcode;
    }
    
	// Status
	public void setStatus(int status)
	{
        this.status.set(status);
    }
	
    public int getStatus() 
    {
        return statusProperty().get();
    }
    
    public IntegerProperty statusProperty()
    {
        return status;
    }
    
    // Time Scanned
    public void setTimeScanned(String scanned)
    {
        this.timeScanned.set(scanned);
    }
    
    public String getTimeScanned() 
    {
        return timeScannedProperty().get();
    }
 
    public StringProperty timeScannedProperty() 
    {
        return timeScanned;
    }
    
    // Print job requested time
    public void setPrintJobRequestedTime(String timeRequested)
    {
        this.printJobRequestedTime.set(timeRequested);
    }
    
    public String getPrintJobRequestedTime() 
    {
        return printJobRequestedTimeProperty().get();
    }
 
    public StringProperty printJobRequestedTimeProperty() 
    {
        return printJobRequestedTime;
    }
    
    // Print job start time
    public void setPrintJobStartTime(String timeStart)
    {
        this.printJobStartTime.set(timeStart);
    }
    
    public String getPrintJobStartTimee() 
    {
        return printJobStartTimeProperty().get();
    }
 
    public StringProperty printJobStartTimeProperty() 
    {
        return printJobStartTime;
    }
    
    // Print job complete time
    public void setPrintJobCompleteTime(String timeComplete)
    {
        this.printJobCompleteTime.set(timeComplete);
    }
    
    public String getPrintJobCompleteTime() 
    {
        return printJobCompleteTimeProperty().get();
    }
 
    public StringProperty printJobCompleteTimeProperty() 
    {
        return printJobCompleteTime;
    }
    
    // Time PS inserted
    public void setTimePsInsert(String psInsert)
    {
        this.timePsInsert.set(psInsert);
    }
    
    public String getTimePsInsert() 
    {
        return timePsInsertProperty().get();
    }
 
    public StringProperty timePsInsertProperty() 
    {
        return timePsInsert;
    }
    
    // MM string time requested
    public void setTimeMmStringRequested(String mmStringRequested)
    {
        this.timeMmStringRequest.set(mmStringRequested);
    }
    
    public String getTimeMmStringRequested() 
    {
        return timeMmStringRequestedProperty().get();
    }
 
    public StringProperty timeMmStringRequestedProperty() 
    {
        return timeMmStringRequest;
    }
    
    // MM string time returned
    public void setTimeMmStringReturn(String mmStringReturn)
    {
        this.timeMmStringReturn.set(mmStringReturn);
    }
    
    public String getTimeMmStringReturn() 
    {
        return timeMmStringReturnProperty().get();
    }
 
    public StringProperty timeMmStringReturnProperty() 
    {
        return timeMmStringReturn;
    }
    
    // MM string value
    public void setMmStringValue(String mmStringValue)
    {
        this.mmStringValue.set(mmStringValue);
    }
    
    public String getMmStringValue() 
    {
        return mmStringValueProperty().get();
    }
 
    public StringProperty mmStringValueProperty() 
    {
        return mmStringValue;
    }
    
    // Time MM 1 inserted
    public void setTimeMm1Inserted(String mm1)
    {
        this.timeMm1Inserted.set(mm1);
    }
    
    public String getTimeMm1Inserted() 
    {
        return timeMm1InsertedProperty().get();
    }
 
    public StringProperty timeMm1InsertedProperty() 
    {
        return timeMm1Inserted;
    }
    
    // Time MM 2 inserted
    public void setTimeMm2Inserted(String mm2)
    {
        this.timeMm2Inserted.set(mm2);
    }
    
    public String getTimeMm2Inserted() 
    {
        return timeMm2InsertedProperty().get();
    }
 
    public StringProperty timeMm2InsertedProperty() 
    {
        return timeMm2Inserted;
    }
    
    // Time MM 3 inserted
    public void setTimeMm3Inserted(String mm3)
    {
        this.timeMm3Inserted.set(mm3);
    }
    
    public String getTimeMm3Inserted() 
    {
        return timeMm3InsertedProperty().get();
    }
 
    public StringProperty timeMm3InsertedProperty() 
    {
        return timeMm3Inserted;
    }
    
    // Time MM 4 inserted
    public void setTimeMm4Inserted(String mm4)
    {
        this.timeMm4Inserted.set(mm4);
    }
    
    public String getTimeMm4Inserted() 
    {
        return timeMm4InsertedProperty().get();
    }
 
    public StringProperty timeMm4InsertedProperty() 
    {
        return timeMm4Inserted;
    }
    
    // Time MM 5 inserted
    public void setTimeMm5Inserted(String mm5)
    {
        this.timeMm5Inserted.set(mm5);
    }
    
    public String getTimeMm5Inserted() 
    {
        return timeMm5InsertedProperty().get();
    }
 
    public StringProperty timeMm5InsertedProperty() 
    {
        return timeMm5Inserted;
    }
}
