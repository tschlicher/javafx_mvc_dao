package Util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.staples.printnpack.Main;
import com.staples.printnpack.PropertiesCache;

/**
 * Comments: This is the Database Utility class which uses the DAO principle and 
 * 			 will have methods responsible for:
 * 			 	1) Connecting to a SQL Server Database
 * 			 	2) Closing a SQL Server Database
 * 			 	3) Execute a query and return a ResultSet
 * 			 	4) Update, Insert or Delete operations
 * 
 * Date     5 Feb 2019
 * @author  Thomas Schlicher
 *
 */

public class DBUtil 
{
	 private static final Logger LOGGER = LogManager.getLogger(Main.class.getName());
	
	 private static final String connStr = PropertiesCache.getInstance().getProperty("db.conn.url");
	 
	 private static final String connStr2 = PropertiesCache.getInstance().getProperty("db.conn.url2");
	 
	 private static final String JDBC_DRIVER = PropertiesCache.getInstance().getProperty("db.driver");
	 
	 private final String uName = PropertiesCache.getInstance().getProperty("db.user");
	 private final String pWord = PropertiesCache.getInstance().getProperty("db.password");
	 
	// private static Connection conn = null;
	 
	 private static DBUtil ds;
	 private static DBUtil ds2;
	 private BasicDataSource basicDS = new BasicDataSource();
	 
	 private DBUtil()
	 {
		 //BasicDataSource basicDS = new BasicDataSource();
		  basicDS.setDriverClassName(JDBC_DRIVER);
		  basicDS.setUsername(uName);
		  basicDS.setPassword(pWord);
		  basicDS.setUrl(connStr);
		  
		  // Parameters for connection pooling
		  basicDS.setInitialSize(30);
		  basicDS.setMaxTotal(30);
	 }
	 
	 private DBUtil(String connection)
	 {
		 //BasicDataSource basicDS = new BasicDataSource();
		  basicDS.setDriverClassName(JDBC_DRIVER);
		  basicDS.setUsername(uName);
		  basicDS.setPassword(pWord);
		  basicDS.setUrl(connection);
		  
		  // Parameters for connection pooling
		  basicDS.setInitialSize(20);
		  basicDS.setMaxTotal(20);
	 }
	 
	 /**
	   *static method for getting instance.
	 */
	 // Non WES sites connections only
	 public static DBUtil getInstance(){
	    if(ds == null){
	        ds = new DBUtil();
	    }
	    return ds;
	 }	 
	 
	 // WES sites DB connection only
	 public static DBUtil getInstance2(String connString){
		 if(ds2 == null){
		     ds2 = new DBUtil(connString);
		 }
		 return ds2;
	 }

	 public BasicDataSource getBasicDS() {
	  return basicDS;
	 }

	 public void setBasicDS(BasicDataSource basicDS) {
	  this.basicDS = basicDS;
	 }
	
	// Database query operation that will return a result set
    public static ResultSet dbExecuteQuery(String queryStmt) throws SQLException, ClassNotFoundException 
    {   	
    	Connection conn = null;
    	// Declare statement, resultSet and CachedResultSet as null
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSet crs = null;
        
        try {
        	
            // Connect to DB (Establish SQL Connection)
        	BasicDataSource basicDS = DBUtil.getInstance().getBasicDS();
	    	   conn = basicDS.getConnection();
 
            // Create statement
            stmt = conn.createStatement();

            // Execute select (query) operation
            resultSet = stmt.executeQuery(queryStmt);
            
            // Interface used to obtain a RowSet implementation
        	RowSetFactory factory = RowSetProvider.newFactory();
        	
            // Using RowSetFactory to populate a CachedRowSet
            crs = factory.createCachedRowSet();
            crs.populate(resultSet);
        } catch (SQLException e) {
        	LOGGER.error("Problem occurred at executeQuery operation : " + e);
            throw e;
        } finally {
            if (resultSet != null) {
                // Close resultSet
                resultSet.close();
            }
            if (stmt != null) {
                // Close Statement
                stmt.close();
            }
            // Close connection
           // dbDisconnect();
            conn.close();
        }
        // Return CachedRowSet
        return crs;
    }
 
    // Performs Update, Insert or Delete operations on the SQL Server Database
    public static void dbExecuteUpdate(String sqlStmt) throws SQLException, ClassNotFoundException 
    {
    	Connection conn = null;
        // Declare statement as null
        Statement stmt = null;
        
        try {
        	
            // Connect to DB (Establish SQL Connection)
        	BasicDataSource basicDS = DBUtil.getInstance().getBasicDS();
	    	   conn = basicDS.getConnection();
            
            //Create Statement
            stmt = conn.createStatement();
            //Run executeUpdate operation with given sql statement
            stmt.executeUpdate(sqlStmt);
            
        } catch (SQLException e) {
            System.out.println("Problem occurred at executeUpdate operation : " + e);
            throw e;
        } finally {
            if (stmt != null) {
                // Close statement
            	System.out.println("Closing statment");
                stmt.close();
            }
            // Close connection
            System.out.println("Closing db connection");
            //dbDisconnect();
            conn.close();
        }
    }
    
 // Database query operation that will return a String
    public static ResultSet dbExecutePdfQuery(String queryStmt) throws SQLException, ClassNotFoundException 
    {   	
    	Connection conn = null;
    	// Declare statement, resultSet and CachedResultSet as null
        Statement stmt = null;
        ResultSet resultSet = null;
        CachedRowSet crs = null;
        
        try {
        	
            // Connect to DB (Establish SQL Connection)
        	BasicDataSource basicDS = DBUtil.getInstance2(connStr2).getBasicDS();
	    	   conn = basicDS.getConnection();
 
            // Create statement
            stmt = conn.createStatement();

            // Execute select (query) operation
            resultSet = stmt.executeQuery(queryStmt);
            
            // Interface used to obtain a RowSet implementation
        	RowSetFactory factory = RowSetProvider.newFactory();
        	
            // Using RowSetFactory to populate a CachedRowSet
            crs = factory.createCachedRowSet();
            crs.populate(resultSet);
        } catch (SQLException e) {
        	LOGGER.error("Problem occurred at executeQuery operation : " + e);
            throw e;
        } finally {
            if (resultSet != null) {
                // Close resultSet
                resultSet.close();
            }
            if (stmt != null) {
                // Close Statement
                stmt.close();
            }
            // Close connection
           // dbDisconnect();
            conn.close();
        }
        // Return CachedRowSet
        return crs;
    }
}
