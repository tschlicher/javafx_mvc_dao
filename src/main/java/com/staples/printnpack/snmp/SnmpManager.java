package com.staples.printnpack.snmp;

import java.io.IOException;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import com.staples.printnpack.PropertiesCache;
import com.staples.printnpack.model.HandlePrinterStatus;
import com.staples.printnpack.model.OrderTrackingDAO;

import Util.PrinterConstants;

/**
 *     Author: Thomas Schlicher
 *       Date: 1/16/2019
 *   
 *   comments: This class will be used to find out if a printer isAvailable and ready to accept a print job by using a 3rd party 
 *   			library (SNMP4j) which will allow communication to printers via SNMP over UDP.
 *   
 *   	  Uses:	
 *   			SNMP4j   - A 3rd party library that allows Java to communicate via SNMP	
 *   			OID's    - Translators that helps a Management Station to understand SNMP responses obtained from the network devices.
 *   			SNMP     - Simple Network Management Protocol (SNMP) is a set of protocols for network management and monitoring
 *   			PDU's    - SNMP PDUs (Protocol data units) are used for communication between SNMP managers and SNMP agents
 *   			Port 161 - Used for reads
 *   			Port 161 - used for writes
 *   			 
 */

public class SnmpManager 
{
	private static final Logger LOGGER = LogManager.getLogger(SnmpManager.class.getName());
	private String[] ipAddressArr =  PropertiesCache.getInstance().getProperty("printerArr").split(",");
	private String oidValue = PropertiesCache.getInstance().getProperty("oid.value");
	private final String PRINTER_OFFLINE = PropertiesCache.getInstance().getProperty("printer.offline.code");
	private final String RESPONSE_NULL = PropertiesCache.getInstance().getProperty("printer.offline");
	static Snmp snmp = null;
	String address = null;

	/**
	 * Comments: Default Constructor
	 */
	public SnmpManager()
	{
		
	}

	/**
	* Comments: Constructor with parameter - the parameter will be a string containing UDP, IP address of printer and port 161
	* 
	* @param add
	*/
	public SnmpManager(String udpIpPort)
	{
		address = udpIpPort;
	}
	
	/**
	 * Comments: Loop through all IP addresses and retrieve data on the printer status
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void initializeSnmpClient()
	{
		// For each printer IP we need to read in the OID value
		for(String ip : ipAddressArr)
		{
			System.out.println("all IPs : " + ip);
			
			//Instantiating this class and passing in our udp + ip + port string 
			SnmpManager client = new SnmpManager("udp:"+ip+"/161");
			try {
					
				client.start();
				System.out.println("Client has been started " + ip);
					
				String sysDescr = client.getAsString(new OID(oidValue));
				
				if(sysDescr.equalsIgnoreCase(RESPONSE_NULL)) 
				{
					// A null response was returned from printer (printer is powered off)
					printerCase(ip, PRINTER_OFFLINE);
				}else{
					// A non-null response was returned from printer (printer is powered on)
					printerCase(ip, sysDescr);
				}		
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}				
		}	
	}

	/**
	* Comments: Start the SNMP session. 
	 * @return 
	* 
	* @throws IOException
	*/
	private void start() throws IOException
	{
		// Choosing UDP to transport data
		TransportMapping<?> transport = new DefaultUdpTransportMapping();
		snmp = new Snmp(transport);

		// Listen allows us to get data from the printers
		transport.listen();
	}

	/**
	* Comments: Method which takes a single OID and returns the response from the printers as a String.
	* 
	* @param oid
	* @return
	* @throws IOException
	*/
	String getAsString(OID oid) throws IOException 
	{
		System.out.println("Made it into the responseevent method");
		ResponseEvent event = get(new OID[] { oid });
		
		if(event == null) {
			LOGGER.info("Class: SnmpGetExample - Response event returned from printer was null, the printer is powered off");
			System.out.println("Printer is null");
			return "null";
		}
		
		System.out.println("Event status: " + event.getResponse().toString());
		snmp.close();

		LOGGER.info("Class: SnmpGetExample - Response event returned from printer is a valid response " + event.getResponse().get(0).getVariable().toString());
		System.out.println("Printers are not null");
		return event.getResponse().get(0).getVariable().toString();			
	}

	/**
	* This method is capable of handling multiple OIDs
	* 
	* @param oids
	* @return
	* @throws IOException
	*/
	private ResponseEvent get(OID oids[]) throws IOException
	{
		LOGGER.info("Class: SnmpGetExample - Instaniating new PDU for SNMP");
		PDU pdu = new PDU();
		
		// Used if there are multiple OIDs or in the future there will be multiple OIDs
		for (OID oid : oids) 
		{
			pdu.add(new VariableBinding(oid));
		}
		
		// Setting PDU type to GET data from printer
		pdu.setType(PDU.GET);
		
		// Get response from printer
		ResponseEvent event = snmp.send(pdu, getTarget(), null);
		LOGGER.info("Class: SnmpGetExample - Received response event");
		
		// Set that response to a PDU to determine if a printer has returned NULL which means the printer is offline
		PDU response = event.getResponse();
		
		if(response == null) {
			LOGGER.info("Class: SnmpGetExample - PDU response event was null, printer is offline");
			System.out.println("Event was null");
			return null;
		}
		LOGGER.info("Class: SnmpGetExample - Retrieved a successful response from printer");
		return event;
	}

	/**
	* Comments: This method returns a Target, which contains information about
	* 			where the data should be fetched and how.
	* 
	* @return
	*/
	private Target getTarget() 
	{
		Address targetAddress = GenericAddress.parse(address);
		CommunityTarget target = new CommunityTarget();
		target.setCommunity(new OctetString("public"));
		target.setAddress(targetAddress);
		target.setRetries(2);
		target.setTimeout(1000);
		target.setVersion(SnmpConstants.version1);
		return target;
	}
	
	/**
	 * Comments: Method used to hold a switch statement that will constantly update the statuses of
	 * 		   each printer based on printer IP address and status code
	 * 
	 * @param printIp
	 * @throws InterruptedException 
	 */
	  private void printerCase(String printIp, String status) throws InterruptedException
	  {
		  HandlePrinterStatus hps = new HandlePrinterStatus();
		  LOGGER.info("Printer IP - " + printIp);
		  
		  // Printers change for each site
		  final String printer1 = PrinterConstants.CHARLOTTE_PRINTER1;
		  final String printer2 = PrinterConstants.CHARLOTTE_PRINTER2;
		  final String printer3 = PrinterConstants.CHARLOTTE_PRINTER3;
		  final String printer4 = PrinterConstants.CHARLOTTE_PRINTER4;
		  final String printer5 = PrinterConstants.CHARLOTTE_PRINTER5;
		  final String hospitalPrinter = PrinterConstants.CHARLOTTE_HOSPITAL_PRINTER_LINE1;

		  switch(printIp)
		  {
		  		case printer1:
		  			LOGGER.info("Class: SnmpGetExample - Setting printer 2 " +printIp+ " status: " + status);
		  			String[] p1Arr = hps.isPrinterAvailable(status);

		  			try {
		  				System.out.println("Calling method to update printer status in the DB");
		  				insertPrinterStatus(printIp, status, p1Arr[0], p1Arr[1], p1Arr[2]);
		  			} catch (ClassNotFoundException | SQLException e) {
		  				LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
		  				e.printStackTrace();
		  			}
		  			break;
		  			
		  		case printer2:
					LOGGER.info("Class: SnmpGetExample - Setting printer 2 " +printIp+ " status: " + status);
					String[] p2Arr = hps.isPrinterAvailable(status);

					try {
						System.out.println("Calling method to update printer status in the DB");
						insertPrinterStatus(printIp, status, p2Arr[0], p2Arr[1], p2Arr[2]);
					} catch (ClassNotFoundException | SQLException e) {
						LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
						e.printStackTrace();
					}
					break;
					
				case printer3:
					LOGGER.info("Class: SnmpGetExample - Setting printer 3 " +printIp+ " status: " + status);;
					String[] p3Arr = hps.isPrinterAvailable(status);

					try {
						System.out.println("Calling method to update printer status in the DB");
						insertPrinterStatus(printIp, status, p3Arr[0], p3Arr[1], p3Arr[2]);
					} catch (ClassNotFoundException | SQLException e) {
						LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
						e.printStackTrace();
					}
					break;
					
				case printer4:
		  			LOGGER.info("Class: SnmpGetExample - Setting printer 4 " +printIp+ " status: " + status);
					String[] p4Arr = hps.isPrinterAvailable(status);

					try {
						System.out.println("Calling method to update printer status in the DB");
						insertPrinterStatus(printIp, status, p4Arr[0], p4Arr[1], p4Arr[2]);
					} catch (ClassNotFoundException | SQLException e) {
						LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
						e.printStackTrace();
					}
					break;
					
				case printer5:
					LOGGER.info("Class: SnmpGetExample - Setting printer 5 " +printIp+ " status: " + status);
					String[] p5Arr = hps.isPrinterAvailable(status);

					try {
						System.out.println("Calling method to update printer status in the DB");
						insertPrinterStatus(printIp, status, p5Arr[0], p5Arr[1], p5Arr[2]);
					} catch (ClassNotFoundException | SQLException e) {
						LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
						e.printStackTrace();
					}
					break;
				
				case hospitalPrinter:
					LOGGER.info("Class: SnmpGetExample - Setting hospital lane printer " +printIp+ " status: " + status);
					String[] hpArr = hps.isPrinterAvailable(status);

					try {
						System.out.println("Calling method to update printer status in the DB");
						insertPrinterStatus(printIp, status, hpArr[0], hpArr[1], hpArr[2]);
					} catch (ClassNotFoundException | SQLException e) {
						LOGGER.error("Class: SnmpGetExample - Printer status was not set for printer: " + printIp);
						e.printStackTrace();
					}
					break;
		  }
	  }
	  
	  /**
	   * Comments: This method takes data for each printer and calls another method that sets all data to DB 
	   * 
	   * @param printerIp
	   * @param printStatus
	   * @param printerStatusColor
	   * @param printerAvailability
	   * @param printerMessage
	   * @throws SQLException
	   * @throws ClassNotFoundException
	   */
	  private void insertPrinterStatus (String printerIp, String printStatus, 
			  String printerStatusColor, String printerAvailability, String printerMessage)throws SQLException, ClassNotFoundException 
	  {
	        try {
	        	System.out.println("Sending printer status to the database for updating");
	        	OrderTrackingDAO.insertPrinterStatus(printerIp, printStatus, printerStatusColor, printerAvailability, printerMessage);
	        } catch (SQLException e) {
	            LOGGER.error("Class: SnmpGetExample - SQL Eception, Printer status was not set for printer: " + e);
	            throw e;
	        }
	    }
}
