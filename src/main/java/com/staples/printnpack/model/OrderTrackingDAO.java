package com.staples.printnpack.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import Util.DBUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *     Author: Thomas Schlicher
 *       Date: 12 Feb 2019
 *   
 *   comments: This class will be used to hold methods that will execute CRUD DB Operations on the Orders table.
 *   			 
 */

public class OrderTrackingDAO 
{
	//*******************************
    //SELECT an Order
    //*******************************
    public static OrderTracking searchOrderTracking (String barCode) throws SQLException, ClassNotFoundException 
    {
        //Declare a SELECT statement
        //String selectStmt = "SELECT * FROM printers WHERE printerId= '"+barCode+"';";
    	
    	// Beloit
    	//String selectStmt = "SELECT * FROM orders WHERE printerId= '"+barCode+"';";
    	
    	// Dallas
        //String selectStmt = "SELECT * FROM ELI WHERE printerId= '"+barCode+"';";
        
        // Charlotte
    	String selectStmt = "SELECT * FROM ELI WHERE printerId= '"+barCode+"';";
 
        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
            ResultSet rsEmp = DBUtil.dbExecuteQuery(selectStmt);
 
            //Send ResultSet to the getOrderFromResultSet method and get orders object
            OrderTracking order = getOrderFromResultSet(rsEmp);
 
            //Return employee object
            return order;
        } catch (SQLException e) {
            System.out.println("While searching an order with " + barCode + " barcode, an error occurred: " + e);
            //Return exception
            throw e;
        }
    }
    
  //Use ResultSet from DB as parameter and set Order Object's attributes and return order object.
    private static OrderTracking getOrderFromResultSet(ResultSet rs) throws SQLException
    {
    	OrderTracking ord = null;
        if (rs.next()) {
        	ord = new OrderTracking();
        	ord.setPrinterId(rs.getString("printerId"));
            ord.setStatus(rs.getString("printerStatus"));
            ord.setStatusColor(rs.getString("printerStatusColor"));
            ord.setPrinterAvailability(rs.getString("printerAvailability"));
            ord.setPrinterMessage(rs.getString("printerMessage"));
        }
        return ord;
    }
    
    //*******************************
    //SELECT all Printers
    //*******************************
    public static ObservableList<OrderTracking> searchAllOrderTracking () throws SQLException, ClassNotFoundException 
    {
        //Declare a SELECT statement
    	// SoCal
        //String selectStmt = "SELECT * FROM PrintnPack.dbo.printers;";
    	
    	// Beloit
    	 //String selectStmt = "SELECT * FROM ELI.dbo.orders;";
    	 
        // WES Sites
        String selectStmt = "SELECT * FROM ELI.dbo.printers;";
 
        //Execute SELECT statement
        try {
            //Get ResultSet from dbExecuteQuery method
        	System.out.println("Executing DB Query: " + selectStmt);
            ResultSet rsOrder = DBUtil.dbExecuteQuery(selectStmt);
            //System.out.println("Resultset is: " + rsOrder);
 
            //Send ResultSet to the getOrderTrackingList method and get employee object
            ObservableList<OrderTracking> orderList = getOrderTrackingList(rsOrder);
            System.out.println("");
            //Return employee object
            return orderList;
        } catch (SQLException e) {
            System.out.println("SQL select operation has been failed: " + e);
            //Return exception
            throw e;
        }
    }
    
    //Select * from Orders
    private static ObservableList<OrderTracking> getOrderTrackingList(ResultSet rs) throws SQLException, ClassNotFoundException 
    {
        //Declare a observable List which comprises of Employee objects
        ObservableList<OrderTracking> orderList = FXCollections.observableArrayList();
 
        while (rs.next()) {        	
        	OrderTracking order = new OrderTracking();
        	order.setPrinterId(rs.getString("printerId"));
        	//System.out.println("get printer id property: " + order.printerIdProperty());
        	
        	order.setStatus(rs.getString("printerStatus"));
        	//System.out.println("get printer status property: " + order.statusProperty());
        	
        	order.setStatusColor(rs.getString("printerStatusColor"));
        	//System.out.println("get printer status color property: " + order.statusColorProperty());
        	
        	order.setPrinterAvailability(rs.getString("printerAvailability"));
        	//System.out.println("get printer availability property: " + order.printerAvailabilityProperty());
        	
        	order.setPrinterMessage(rs.getString("printerMessage"));
        	//System.out.println("get printer message property: " + order.printerMessageProperty());
        	
            //Add orders to the ObservableList
            orderList.add(order);
        }
        //return order List (ObservableList of Employees)
        return orderList;
    }
    
    //*************************************
    //DELETE a CartonId after reprint
    //*************************************
    public static void deleteOrderWithBarcode (String barCode) throws SQLException, ClassNotFoundException 
    {
        //Declare a DELETE statement
        String updateStmt = "DELETE FROM printers WHERE barcode ="+ barCode;
 
        //Execute UPDATE operation
        try {
            DBUtil.dbExecuteUpdate(updateStmt);
        } catch (SQLException e) {
            System.out.print("Error occurred while DELETE Operation: " + e);
            throw e;
        }
    }
    
  //*************************************
    //Update a printer status into the printers table
    //*************************************
    public static void insertPrinterStatus (String pName, String status, String color, String availability, String msg) throws SQLException, ClassNotFoundException 
    {
        //Declare a INSERT statement
    	// SoCal
    	System.out.println("Setting query statement");
       // String updateStmt = "UPDATE PrintnPack.dbo.printers SET printerStatus = '"+status+"', printerStatusColor = '"+color+"', "
        		//+ "printerAvailability = '"+availability+"', printerMessage = '"+msg+"' WHERE printerId = '"+pName+"';";
    	
    	// Beloit
    	//String updateStmt = "UPDATE ELI.dbo.orders SET printerStatus = '"+status+"', printerStatusColor = '"+color+"', "
        		//+ "printerAvailability = '"+availability+"', printerMessage = '"+msg+"' WHERE printerId = '"+pName+"';";
    	
        // Dallas - Lithia
        String updateStmt = "UPDATE ELI.dbo.printers SET printerStatus = '"+status+"', printerStatusColor = '"+color+"', "
        		+ "printerAvailability = '"+availability+"', printerMessage = '"+msg+"' WHERE printerId = '"+pName+"';";
 
        System.out.println("Getting ready to update database");
        
        //Execute Update operation
        try {
        	System.out.println("Updating database with printer status");
            DBUtil.dbExecuteUpdate(updateStmt);
            System.out.println("Update completed");
        } catch (SQLException e) {
            System.out.print("Error occurred while UPDATE Operation: " + e);
            throw e;
        }
    }
}
